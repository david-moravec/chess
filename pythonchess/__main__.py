import pygame
import os
import inspect

from pythonchess.src.board import Board
from pythonchess.src.constants import HEIGHT, WIDTH, SQUARE_SIZE
from pythonchess.src.game import Game
from pythonchess.src.piece import Position

os.environ["PYTHONBREAKPOINT"] = "0"


DEBUG = False

FPS = 60
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Chess")

def get_row_col_from_mouse(pos):
    x, y = pos
    row = x // SQUARE_SIZE
    col = y // SQUARE_SIZE
    position = Position(row=row, column=col)
    return position

def main():
    run = True
    clock = pygame.time.Clock()
    game = Game(WIN)
    game.update()

    while run:
        clock.tick(FPS)

#        if game.winner() != None:
#            print(game.winner())
#            run = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                position = get_row_col_from_mouse(pos)
                game.evaluate_click(position)
                game.update()
                #game._printBoard()


if __name__ == "__main__":
    main()

