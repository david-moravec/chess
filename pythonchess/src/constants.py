from pythonchess.src.piece import PieceType, Piece

piece_types: list[type[Piece]] = Piece.__subclasses__()

CHAR_2_TYPE = dict(zip([piece.value for piece in PieceType], piece_types))

WIDTH, HEIGHT = 400, 400
SQUARE_SIZE = 50
SCALE_FACTOR = (40, 40)
WHITE = (211,211,211)
BLUE = (100,149,237)
GREEN = (0, 128, 0)
ROWS = 8
COLS = 8
DEBUG = False
