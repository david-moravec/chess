import pygame
from typing import Optional

from pythonchess.src.board import Board
from pythonchess.src.piece import Piece, Position
from pythonchess.src.constants import WHITE, BLUE, DEBUG, ROWS, COLS, SQUARE_SIZE, GREEN

#interface between pyGame and board

DEBUG = False


class Game():
    def __init__(self, win):
        super().__init__()
        self._selected: Optional[Piece] = None
        self.turn: tuple = WHITE
        self.win = win
        self._board = Board()

    @property
    def selected(self) -> Optional[Piece]:
        return self._selected

    @property
    def board(self) -> Board:
        return self._board

    def update(self):
        self._draw_squares()
        self._draw_valid_moves()
        self._draw_pieces()
        pygame.display.update()

    def _draw_squares(self):
        win = self.win

        win.fill(WHITE)
        for row in range(ROWS):
            for col in range(row % 2, COLS, 2):
                pygame.draw.rect(win, BLUE, (row*SQUARE_SIZE, col *SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))

    def _draw_valid_moves(self):
        win = self.win

        for key in self.board.valid_moves.keys():
            for move in self._valid_moves.get(key, []):
                row = SQUARE_SIZE * move[1] + SQUARE_SIZE//2
                col = SQUARE_SIZE * move[0] + SQUARE_SIZE//2
                pygame.draw.circle(win, GREEN, (row, col), 15)

    def _draw_pieces(self):
        win = self.win

        for piece in self.board.pieces:
            if piece.alive:
                win.blit(piece.image(), piece.position.square_center.to_tuple())
            else:
                continue

    def _move_piece(self, move):
        for key in self._valid_moves.keys():
            if move in self._valid_moves[key]:
                self._selected.move(move) # type: ignore
                for piece in self._pieces:
                    if(piece.position == move and piece.team != self.turn):
                        piece.die()

    def evaluate_click(self, position: Position):
        #if we already selected a piece, we want to move it. Currently does not support change of pieces, so if we choose a piece we HAVE to move it
        if self.selected:
            self._move_piece(position)
            if self.selected.moved:
                self._change_turns()
                self.selected.reset_moved()
                self.reset_valid_moves()

            self.selected = None
            position = None

        #if we have no piece selected, select current piece
        else:
            for piece in self.board.alive_pieces:
                if (piece.position == position and piece.team == self.turn):
                    self.selected = piece
                    self.selected.old_position = position
                    self.board._get_valid_moves(self.selected)

            if DEBUG:
                print(self.evaluate_click.__name__, "\n")
                print("selected Piece:", self.selected)
                print("valid moves", self.valid_moves)



    def _change_turns(self):
        if self.turn == WHITE:
            self.turn = BLUE
        else:
            self.turn = WHITE
