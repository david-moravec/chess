import pygame
import os
from enum import Enum, auto
from typing import Callable


from dataclasses import dataclass
from abc import ABC
from typing import NewType

@dataclass(frozen=True)
class Images:
    black = (pygame.image.load("pythonchess/src/figures-graphics/black-knight.png"),
             pygame.image.load("pythonchess/src/figures-graphics/black-king.png"),
             pygame.image.load("pythonchess/src/figures-graphics/black-queen.png"),
             pygame.image.load("pythonchess/src/figures-graphics/black-bishop.png"),
             pygame.image.load("pythonchess/src/figures-graphics/black-rook.png"),
             pygame.image.load("pythonchess/src/figures-graphics/black-pawn.png"),)

    white = (pygame.image.load("pythonchess/src/figures-graphics/white-knight.png"),
             pygame.image.load("pythonchess/src/figures-graphics/white-king.png"),
             pygame.image.load("pythonchess/src/figures-graphics/white-queen.png"),
             pygame.image.load("pythonchess/src/figures-graphics/white-bishop.png"),
             pygame.image.load("pythonchess/src/figures-graphics/white-rook.png"),
             pygame.image.load("pythonchess/src/figures-graphics/white-pawn.png"),)

class PieceType(Enum):
    PAWN = "p"
    KNIGHT = "n"
    BISHOP = "b"
    ROOK = "r"
    QUEEN = "q"
    KING = "k"

_CHAR_2_PIC = dict(zip((piece.value for piece in PieceType), Images.black))
_CHAR_2_PIC.update(zip((piece.value.upper() for piece in PieceType), Images.white))


@dataclass(frozen=True, kw_only=True)
class Position:
    row: int = 0
    column: int = 0

    @property
    def square_center(self) -> "Position": 
        from pythonchess.src.constants import SCALE_FACTOR, SQUARE_SIZE
        def centered_coordinate(coordinate: int) -> int:
            return int(SQUARE_SIZE * coordinate + (SQUARE_SIZE - SCALE_FACTOR[0]) / 2)

        return Position(row=centered_coordinate(self.row), column=centered_coordinate(self.column))

    def to_tuple(self) -> tuple:
        return (self.column, self.row,)

Moves = NewType("Moves", dict[str,Position])

DEBUG = False

class Piece(ABC):
    def __init__(self, position: Position, team: tuple):
        self._team = team
        self._old_position = Position()
        self._position = position
        self._potential_moves : Moves = {}
        self.moved = False
        self.alive = True

    def __str__(self):
        from pythonchess.src.constants import BLUE

        name: str = self.__class__.__name__
        piecetype: PieceType = PieceType.__dict__[name.upper()]
        char: str = piecetype.value

        return char if self.team == BLUE else char.upper()

    @property
    def position(self) -> Position:
        return (self._position)

    @property
    def old_position(self) -> Position:
        return self._old_position

    @old_position.setter
    def old_position(self, position: Position) -> None:
        self._old_position = position

    @property
    def team(self):
        return self._team

    @property
    def potential_moves(self) -> Moves:
        return self._potential_moves


    def move(self, position) -> None:
        self._position = position
        self.moved = True

    def reset_moved(self) -> None:
        self.moved = False

    def die(self) -> None:
        self.alive = False

    def did_move(self, position: Position) -> bool:
        #print(position, self.oldPosition())
        return position != self.old_position

    def crop_potential_moves(self) -> Moves:
        cropped_moves = {}
        for key in self._potential_moves.keys():
            cropped_moves[key] = []

            for move in self._potential_moves[key]:
                if (   move.row > 7 or move.row < 0 
                    or move.col > 7 or move.col < 0
                   ):
                    continue
                elif (move == self.position):
                    continue
                else:
                    cropped_moves[key].append(move)

        return cropped_moves

    def image(self):
        from pythonchess.src.constants import SCALE_FACTOR

        transform: Callable = pygame.transform.scale
        pic: pygame.sruface = _CHAR_2_PIC[str(self)]

        return transform(pic, SCALE_FACTOR)

class Knight(Piece):
    def __init__(self, position, team):
        Piece.__init__(self, position, team)


    def potential_moves(self):
        self._potential_moves = {'dummy' : []}
        row = self._position.row
        col = self._position.col

        self._potential_moves['dummy'].append(Position(row + 2, col + 1))
        self._potential_moves['dummy'].append(Position(row + 1, col + 2))

        self._potential_moves['dummy'].append(Position(row - 2, col + 1))
        self._potential_moves['dummy'].append(Position(row - 1, col + 2))

        self._potential_moves['dummy'].append(Position(row + 2, col - 1))
        self._potential_moves['dummy'].append(Position(row + 1, col - 2))
        
        self._potential_moves['dummy'].append(Position(row - 2, col - 1))
        self._potential_moves['dummy'].append(Position(row - 1, col - 2))

        return self.crop_potential_moves()

class Bishop(Piece):
    def __init__(self, position, team):
        Piece.__init__(self, position, team)

    def potential_moves(self):
        self._potential_moves = {'xy': [],
                                '-xy': [],
                                'yx': [],
                                '-yx': []
                               }
        row = self._position.row
        col = self._position.col
        for i in range(7):
            self._potential_moves['xy'].append(Position(row + i, col + i))
            self._potential_moves['-yx'].append(Position(row + i, col - i))
            self._potential_moves['yx'].append(Position(row - i, col - i))
            self._potential_moves['-xy'].append(Position(row - i, col + i))

        return self.crop_potential_moves()
        
class Rook(Piece):
    def __init__(self, position, team):
        Piece.__init__(self, position, team)

    def potential_moves(self):
        self._potential_moves = {'x': [],
                                'y': [],
                                '-x': [],
                                '-y': []
                               }
        row = self._position.row
        col = self._position.col
        for i in range(8):
            self._potential_moves['x'].append(Position(row + i, col))
            self._potential_moves['-x'].append(Position(row - i, col))
            self._potential_moves['y'].append(Position(row, col - i))
            self._potential_moves['-y'].append(Position(row, col + i))

        return self.crop_potential_moves()
        pass

class Queen(Piece):
    def __init__(self, position, team):
        Piece.__init__(self, position, team)

    def potential_moves(self):
        self._potential_moves = {}
        bishop_moves = Bishop.potential_moves(self) # type: ignore
        rook_moves = Rook.potential_moves(self) # type: ignore
        for move_set in (bishop_moves, rook_moves):
            self._potential_moves.update(move_set)
        return self.crop_potential_moves()
    
class King(Piece):
    def __init__(self, position, team):
        Piece.__init__(self, position, team)

    def potential_moves(self):
        self._potential_moves['dummy'] = []
        row = self._position.row
        col = self._position.col
        self._potential_moves['dummy'].append(Position(row + 1, col))
        self._potential_moves['dummy'].append(Position(row - 1, col))
        self._potential_moves['dummy'].append(Position(row, col - 1))
        self._potential_moves['dummy'].append(Position(row, col + 1))

        self._potential_moves['dummy'].append(Position(row + 1, col + 1))
        self._potential_moves['dummy'].append(Position(row + 1, col - 1))
        self._potential_moves['dummy'].append(Position(row - 1, col - 1))
        self._potential_moves['dummy'].append(Position(row - 1, col + 1))

        return self.crop_potential_moves()

class Pawn(Piece):
    def __init__(self, position, team):
        Piece.__init__(self, position, team)

    def potential_moves(self):
        pass