import pygame
import enum
from typing import Iterator, Optional

import pythonchess.src.piece as p
from pythonchess.src.piece import Piece, Position
from pythonchess.src.constants import SQUARE_SIZE, SCALE_FACTOR, WHITE, BLUE, ROWS, COLS, GREEN, CHAR_2_TYPE

#stores all the info about pices passes moves from pygame guie to selected piece

#DEBUG = False

class Board:
    def __init__(self) -> None:
        self._board: list = [] 
        self._pieces: tuple[Piece] = () # type: ignore
        self.turn = WHITE
        self._valid_moves = {}
        self.win = None


        self._create_board()

    @property
    def pieces(self) -> tuple[Piece]:
        return self._pieces

    @property
    def alive_pieces(self) -> Iterator[Piece]: # type: ignore
        return (piece for piece in self.pieces if piece.alive)

    @property
    def valid_moves(self) -> dict[str,Position]:
        return self._valid_moves
    
    def _get_piece(self, position) -> Optional[Piece]:
        for piece in self.alive_pieces:
            if piece.position == position:
                return piece

        return None

    def _create_board(self) -> None:

        for row in range(ROWS):
            row = []
            for col in range(COLS):
                row.append(0)
            self._board.append(row)

        self._starting_setup()

    def _get_valid_moves(self, piece: Piece) -> list:
        potential_moves = piece.potential_moves
        for key in potential_moves.keys():
            self._valid_moves[key] = []
            for move in potential_moves[key]:
                target: Optional[Piece] = self._get_piece(move)
                if (type(target) != type(None)):
                    if (target.team() == piece.team()): # type: ignore
                        break
                    elif (target.team() != piece.team()): # type: ignore
                        self._valid_moves[key].append(move)
                        break
                else:
                    self._valid_moves[key].append(move)


    def _reset_valid_moves(self):
        self._valid_moves = {}


    def _starting_setup(self):
        #self._applcolFENposition("1N3/5/5/5/1n3")
        #self._applcolFENposition("4N3/pppppppp/8/8/8/8/pppppppp/3n4")
        self._apply_FEN_position("1RQBNK2/8/8/8/8/8/8/2knbrq1")
        #print(self._piecesposition())
        #self._applcolFENposition("RNBKQBNR/PPPPPPPP/8/8/8/8/pppppppp/rnbkqbnr")


    def _apply_FEN_position(self, fen_string):
        row = 0
        col = 0
        for line in fen_string.split("/"):
            for char in line:
                try:
                    col += int(char)
                except ValueError:
                    position = Position(row=row, column=col)
                    self._init_piece_in_position(char=char, position=position)
                    col+=1
            col = 0
            row+=1

    def _init_piece_in_position(self,* , char: str, position: Position):
        if char.isupper():
            team = WHITE
        else:
            team = BLUE

        piece: type[Piece] = CHAR_2_TYPE[char.lower()]
        self._pieces = self._pieces + (piece(position=position, team=team),)

    def _printBoard(self):
        for row in self._board:
            print(row)
        print("\n")    